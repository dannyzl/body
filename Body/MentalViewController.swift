
//
//  MoodsViewController.swift
//  Body
//
//  Created by Daniel Li on 7/23/15.
//  Copyright (c) 2015 Daniel. All rights reserved.
//

import UIKit
import RealmSwift

class MentalViewController: UIViewController {

    var entry = Entry()
    
    @IBAction func sleepButton(sender: UIButton) {
        displayEditAlert("Sleep")
    }
    
    @IBAction func wakeButton(sender: UIButton) {
        displayEditAlert("Wakeup")
    }
    
    @IBAction func hungerButton(sender: UIButton) {
        displayCommentAlert("Hunger")
    }
    
    @IBAction func headacheButton(sender: UIButton) {
        displayCommentAlert("Headache")
    }
    
    @IBAction func tiredButton(sender: UIButton) {
        displayCommentAlert("Happy")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Display alert with option to add comment
    func displayCommentAlert(entryName: String) {
        entry.name = entryName
        entry.id = entry.newEntryID()
        entry.timeStampNSDate = NSDate()
        
        let alertController = UIAlertController(title: "Add comment?", message: nil, preferredStyle: .Alert)
        
        let commentAction = UIAlertAction(title: "Add", style: .Cancel) { (action) in
            self.performSegueWithIdentifier("commentSegue", sender: self)
        }
        alertController.addAction(commentAction)
        
        let createAction = UIAlertAction(title: "No", style: .Destructive) { (action) in
            let realm = Realm()
            realm.write {
                realm.add(self.entry)
            }
            println("\(entryName) logged with id = \(self.entry.id) and time stamp: \(self.entry.timeStampStringShort)")
            self.performSegueWithIdentifier("unwindHome", sender: self)
        }
        alertController.addAction(createAction)
        
        self.presentViewController(alertController, animated: true) { }
    }
    
    // Display alert with option to edit timestamp
    func displayEditAlert(entryName: String) {
        entry.name = entryName
        entry.id = entry.newEntryID()
        entry.timeStampNSDate = NSDate()
        
        let alertController = UIAlertController(title: entry.timeStampStringShort, message: "Edit timestamp?", preferredStyle: .Alert)
        
        let createAction = UIAlertAction(title: "No", style: .Cancel) { (action) in
            let realm = Realm()
            realm.write {
                realm.add(self.entry)
            }
            println("\(entryName) logged with id = \(self.entry.id) and time stamp: \(self.entry.timeStampStringShort)")
            self.performSegueWithIdentifier("unwindHome", sender: self)
        }
        alertController.addAction(createAction)
        
        let editAction = UIAlertAction(title: "Edit", style: .Default) { (action) in
            self.performSegueWithIdentifier("editSegue", sender: self)
        }
        alertController.addAction(editAction)
        
        self.presentViewController(alertController, animated: true) {
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "editSegue" {
            if let navigationViewController = segue.destinationViewController as? CustomNavigationController {
                let editTimeViewController = navigationViewController.viewControllers.first as! EditTimeViewController
                editTimeViewController.entry = self.entry
            }
        }
        if segue.identifier == "commentSegue" {
            if let navigationViewController = segue.destinationViewController as? CustomNavigationController {
                let addCommentViewController = navigationViewController.viewControllers.first as! AddCommentViewController
                addCommentViewController.entry = self.entry
            }
        }
    }

}
