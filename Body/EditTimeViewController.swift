//
//  EditTimeViewController.swift
//  Body
//
//  Created by Daniel Li on 7/24/15.
//  Copyright (c) 2015 Daniel. All rights reserved.
//

import UIKit
import RealmSwift

class EditTimeViewController: UITableViewController {
    
    var entry = Entry()
    
    let currentDate = NSDate()
    
    @IBAction func cancelButton(sender: UIBarButtonItem) {
        entry.timeStampNSDate = currentDate
        let realm = Realm()
        realm.write {
            realm.add(self.entry)
        }
        println("\(entry.name) logged with id = \(entry.id) and time stamp: \(entry.timeStampStringShort)")
        performSegueWithIdentifier("unwindHome", sender: self)
    }
    @IBAction func doneButton(sender: UIBarButtonItem) {
        let realm = Realm()
        realm.write {
            realm.add(self.entry)
        }
        println("\(entry.name) logged with id = \(entry.id) and time stamp: \(entry.timeStampStringShort)")
        performSegueWithIdentifier("unwindHome", sender: self)
    }
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    
    @IBAction func datePicker(sender: UIDatePicker) {
        timestampLabel.text = NSDateFormatter.localizedStringFromDate(sender.date, dateStyle: .MediumStyle, timeStyle: .ShortStyle)
        entry.timeStampNSDate = sender.date
    }
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var timestampLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Appearance
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
        if let font = UIFont(name: "Avenir", size: 16) {
            cancelButton.setTitleTextAttributes([NSFontAttributeName: font], forState: UIControlState.Normal)
            doneButton.setTitleTextAttributes([NSFontAttributeName: font], forState: UIControlState.Normal)
        }
        
        self.title = "Edit " + entry.name
        datePicker.date = currentDate
        timestampLabel.text = NSDateFormatter.localizedStringFromDate(currentDate, dateStyle: .MediumStyle, timeStyle: .ShortStyle)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 2
    }
}
