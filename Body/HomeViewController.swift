//
//  HomeViewController.swift
//  Body
//
//  Created by Daniel Li on 7/21/15.
//  Copyright (c) 2015 Daniel. All rights reserved.
//

import UIKit
import RealmSwift

class HomeViewController: UIViewController {
    
    var entry = Entry()
    
    @IBAction func foodButton(sender: UIButton) {
        
    }
    
    @IBAction func waterButton(sender: UIButton) {
        // Create object
        entry = Entry()
        entry.name = "Water"
        entry.id = entry.newEntryID()
        entry.timeStampNSDate = NSDate()
        println("Water logged with id = " + entry.id.description + " and time stamp: " + entry.timeStampString)
        
        // Write object to database
        let realm = Realm()
        realm.write {   realm.add(self.entry)   }
        
    }
    
    @IBAction func peeButton(sender: UIButton) {
        // Create object
        entry = Entry()
        entry.name = "Event #1"
        entry.id = entry.newEntryID()
        entry.timeStampNSDate = NSDate()

        let alertController = UIAlertController(title: "Add comment?", message: nil, preferredStyle: .Alert)
        
        let commentAction = UIAlertAction(title: "Add", style: .Cancel) { (action) in
            self.performSegueWithIdentifier("commentSegue", sender: self)
        }
        alertController.addAction(commentAction)
        
        let createAction = UIAlertAction(title: "No", style: .Destructive) { (action) in
            let realm = Realm()
            realm.write {
                realm.add(self.entry)
            }
            println("Pee logged with id = " + self.entry.id.description + " and time stamp: " + self.entry.timeStampString)
        }
        alertController.addAction(createAction)
        
        self.presentViewController(alertController, animated: true) { }
    }
    
    @IBAction func pooButton(sender: UIButton) {
        // Create object
        entry = Entry()
        entry.name = "Event #2"
        entry.id = entry.newEntryID()
        entry.timeStampNSDate = NSDate()
        
        let alertController = UIAlertController(title: "Add comment?", message: nil, preferredStyle: .Alert)
        
        let commentAction = UIAlertAction(title: "Add", style: .Cancel) { (action) in
            self.performSegueWithIdentifier("commentSegue", sender: self)
        }
        alertController.addAction(commentAction)
        
        let createAction = UIAlertAction(title: "No", style: .Destructive) { (action) in
            let realm = Realm()
            realm.write {
                realm.add(self.entry)
            }
            println("Poo logged with id = " + self.entry.id.description + " and time stamp: " + self.entry.timeStampString)
        }
        alertController.addAction(createAction)
        
        self.presentViewController(alertController, animated: true) { }
    }
    
    @IBAction func moodButton(sender: UIButton) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        entry = Entry()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindHome(sender: UIStoryboardSegue) {
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "commentSegue" {
            if let navigationController = segue.destinationViewController as? CustomNavigationController {
                let addCommentViewController = navigationController.viewControllers.first as! AddCommentViewController
                addCommentViewController.entry = self.entry
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
