//
//  ViewAllLogViewController.swift
//  Body
//
//  Created by Daniel Li on 7/21/15.
//  Copyright (c) 2015 Daniel. All rights reserved.
//

import UIKit
import RealmSwift

class ViewAllLogViewController: UITableViewController {
    
    // Sorted entries
    var entries: Results<Entry> {
        get {
            return Realm().objects(Entry).sorted("timeStampNSDate", ascending: false)
        }
    }
    
    // Dates occupied by the entries
    var uniqueDates = [String]()
    
    // 2D holder for entries and thus the tableView model. Outer layer is section, inner is row.
    var datedEntries = [[Entry]]()
    
    // Current selected entry
    var selectedEntry = Entry()
    
    @IBOutlet weak var todayButton: UIBarButtonItem!
    @IBOutlet weak var closeButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
        self.tableView.estimatedRowHeight = 44.0;
        // Customize UIBarButtonItem fonts
        if let font = UIFont(name: "Avenir", size: 16) {
            todayButton.setTitleTextAttributes([NSFontAttributeName: font], forState: UIControlState.Normal)
            closeButton.setTitleTextAttributes([NSFontAttributeName: font], forState: UIControlState.Normal)
        }
        
        // Put all dates into a dates array
        var dateStrings = [String]()
        for entry in entries {
            let dateString = dateToString(entry.timeStampNSDate)
            dateStrings.append(dateString)
        }
        // Remove duplicates
        uniqueDates = uniq(dateStrings)
        
        // Add entries to 2D holder sorted by date
        for dateString in uniqueDates {
            // Temporary array that stores an entry if it matches the date
            var tempArray = [Entry]()
            for entry in entries {
                if dateToString(entry.timeStampNSDate) == dateString {
                    tempArray.append(entry)
                }
            }
            // Add temp array to the 2D stack
            datedEntries.append(tempArray)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Customize header
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView,
        forSection section: Int) {
            // Text Color
            let header = view as! UITableViewHeaderFooterView
            header.textLabel.font = UIFont(name: "Book Antiqua", size: 16)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return uniqueDates.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return datedEntries[section].count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let selectedEntryIndex = find(datedEntries[indexPath.section], selectedEntry) {
            if indexPath.row == selectedEntryIndex + 1 {
                let cell = tableView.dequeueReusableCellWithIdentifier("commentsCell", forIndexPath: indexPath) as! CommentsCell
                cell.commentsLabel!.text = selectedEntry.comments
                cell.userInteractionEnabled = false
                return cell
            }
        }
        let entry = datedEntries[indexPath.section][indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("entryCell", forIndexPath: indexPath) as! EntryCell
        cell.nameLabel?.text = entry.name
        cell.timeLabel?.text = entry.timeStampTimeString
        cell.backgroundColor = UIColor.whiteColor()
        cell.accessoryType = .None
        cell.selectionStyle = .None
        cell.commentsImage.hidden = true
        if !entry.comments.isEmpty {
            cell.commentsImage.hidden = false
            cell.selectionStyle = .Default
        }
        if let selectedEntryIndex = find(datedEntries[indexPath.section], selectedEntry) {
            if indexPath.row == selectedEntryIndex {
                cell.separatorInset = UIEdgeInsetsMake(0, 0, cell.bounds.size.height, cell.bounds.size.width)
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let calendar = NSCalendar.currentCalendar()
        let yesterday = calendar.dateByAddingUnit(.CalendarUnitDay, value: -1, toDate: NSDate(), options: nil)
        
        let todayString = dateToString(NSDate())
        let yesterdayString = dateToString(yesterday!)
        if uniqueDates[section] == todayString {
            return "Today"
        }
        if uniqueDates[section] == yesterdayString {
            return "Yesterday"
        }
        return uniqueDates[section]
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        // If there's row that's already selected...
        if let selectedIndexPath = tableView.indexPathForSelectedRow() {
            // Deselect the row that's already selected...
            tableView.deselectRowAtIndexPath(selectedIndexPath, animated: true)
            
            // Delete comments row below it from the table
            let deleteIndexPath = NSIndexPath(forRow: selectedIndexPath.row + 1, inSection: selectedIndexPath.section)
            datedEntries[selectedIndexPath.section].removeAtIndex(selectedIndexPath.row + 1)
            tableView.deleteRowsAtIndexPaths([deleteIndexPath], withRowAnimation: .Top)
            
            // And set the selected entry to default
            selectedEntry = Entry()
            
            // Don't select it
            return nil
        }
        if datedEntries[indexPath.section][indexPath.row].comments.isEmpty {
            return nil
        }
        // Otherwise, select the row.
        selectedEntry = datedEntries[indexPath.section][indexPath.row]
        return indexPath
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if !selectedEntry.comments.isEmpty {
            let insertIndexPath = NSIndexPath(forRow: indexPath.row + 1, inSection: indexPath.section)
            datedEntries[indexPath.section].insert(Entry(), atIndex: indexPath.row + 1)
            tableView.insertRowsAtIndexPaths([insertIndexPath], withRowAnimation: .Top)
        }
    }
    
    func dateToString(date: NSDate) -> String {
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.CalendarUnitMonth | .CalendarUnitDay | .CalendarUnitWeekday, fromDate: date)
        let weekdayString = NSDateFormatter().weekdaySymbols[components.weekday - 1] as! String
        let monthString = NSDateFormatter().monthSymbols[components.month - 1] as! String
        let dayInt = components.day
        return weekdayString + ", " + monthString + " \(dayInt)"
    }
    
    func uniq<S : SequenceType, T : Hashable where S.Generator.Element == T>(source: S) -> [T] {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
    
}
