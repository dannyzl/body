//
//  Entry.swift
//  Body
//
//  Created by Daniel Li on 7/21/15.
//  Copyright (c) 2015 Daniel. All rights reserved.
//

import Foundation
import RealmSwift

class Entry: Object {
    dynamic var timeStampNSDate: NSDate = NSDate()
    dynamic var timeStampString: String {
        return NSDateFormatter.localizedStringFromDate(timeStampNSDate, dateStyle: .LongStyle, timeStyle: .MediumStyle)
    }
    dynamic var timeStampStringShort : String {
        return NSDateFormatter.localizedStringFromDate(timeStampNSDate, dateStyle: .ShortStyle, timeStyle: .ShortStyle)
    }
    dynamic var timeStampTimeString: String {
        return NSDateFormatter.localizedStringFromDate(timeStampNSDate, dateStyle: .NoStyle, timeStyle: .ShortStyle)
    }
    dynamic var name: String = "Food"
    dynamic var id: Int = -1
    dynamic var comments: String = ""
    override static func primaryKey() -> String? {
        return "id"
    }
    // Returns next available id for an entry
    func newEntryID() -> Int {
        let objects = Realm().objects(Entry)
        if objects.count == 0 {
            return 0
        }
        return objects.sorted("id", ascending: true)[objects.endIndex.predecessor()].id + 1
    }
}