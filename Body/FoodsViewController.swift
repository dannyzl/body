//
//  FoodsViewController.swift
//  Body
//
//  Created by Daniel Li on 7/23/15.
//  Copyright (c) 2015 Daniel. All rights reserved.
//

import UIKit
import RealmSwift

class FoodsViewController: UIViewController {
    
    var entryName: String!
    
    @IBAction func proteinButton(sender: UIButton) {
        entryName = "Protein"
        performSegueWithIdentifier("commentSegue", sender: sender)
    }
    @IBAction func vegetablesButton(sender: UIButton) {
        entryName = "Vegetables"
        performSegueWithIdentifier("commentSegue", sender: sender)
    }
    @IBAction func starchButton(sender: UIButton) {
        entryName = "Starch"
        performSegueWithIdentifier("commentSegue", sender: sender)
    }
    @IBAction func fruitButton(sender: UIButton) {
        entryName = "Fruit"
        performSegueWithIdentifier("commentSegue", sender: sender)
    }
    @IBAction func dairyButton(sender: UIButton) {
        entryName = "Dairy"
        performSegueWithIdentifier("commentSegue", sender: sender)
    }
    @IBAction func sweetsButton(sender: UIButton) {
        entryName = "Sweets"
        performSegueWithIdentifier("commentSegue", sender: sender)
    }
    @IBAction func otherButton(sender: UIButton) {
        entryName = "Other"
        performSegueWithIdentifier("commentSegue", sender: sender)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "commentSegue" {
            if let navigationViewController = segue.destinationViewController as? CustomNavigationController {
                let addCommentViewController = navigationViewController.viewControllers.first as! AddCommentViewController
                addCommentViewController.entry.name = entryName
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
