//
//  SettingsViewController.swift
//  Body
//
//  Created by Daniel Li on 7/24/15.
//  Copyright (c) 2015 Daniel. All rights reserved.
//

import UIKit
import RealmSwift

class SettingsViewController: UITableViewController {
    var times = 0
    @IBOutlet weak var deleteEntryCell: UITableViewCell!
    var messageArray = ["No, seriously, it doesn't.", "Really?", "You must be really bored.", "One more press and I'll crash."]
    @IBOutlet weak var deleteEntriesLabel: UILabel!
    @IBOutlet weak var switchLabel: UILabel!
    @IBAction func switchToggled(sender: UISwitch) {
        switchLabel.text = messageArray[times]
        times++
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Realm().objects(Entry).count > 0 {
            deleteEntryCell.userInteractionEnabled = true
            deleteEntriesLabel.textColor = UIColor.redColor()
        } else {
            deleteEntryCell.userInteractionEnabled = false
            deleteEntriesLabel.textColor = UIColor.grayColor()
        }
        
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 1
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 1 {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            let alertController = UIAlertController(title: "Really delete all logs?", message: "You can't undo this. But you can undo my jeans.", preferredStyle: .ActionSheet)
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            alertController.addAction(cancelAction)
            let deleteAction = UIAlertAction(title: "Delete", style: .Destructive) { (action) in
                let realm = Realm()
                realm.write { realm.deleteAll() }
                println("Deleted all entries in database.")
                self.deleteEntryCell.userInteractionEnabled = false
                self.deleteEntriesLabel.textColor = UIColor.grayColor()
            }
            alertController.addAction(deleteAction)
            presentViewController(alertController, animated: true) { }
            
        }
    }
}
