//
//  AppDelegate.swift
//  Body
//
//  Created by Daniel Li on 7/21/15.
//  Copyright (c) 2015 Daniel. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        println("Application launched")
        clearOldEntries()
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        println("Application about to become inactive")
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        println("Application entered background")
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        println("Application entered inactive state (foreground) from background")
        
        clearOldEntries()
        
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        println("Application became active")
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        println("Application will quit")
    }
    
    func clearOldEntries() {
        let entryCapacity = 175
        let realm = Realm()
        let entries = realm.objects(Entry).sorted("id", ascending: true)
        let numberToDelete = entries.count - entryCapacity
        if numberToDelete > 0 {
            println("Too many entries in memory (\(entries.count)/\(entryCapacity)). Deleting...")
            println("---------------------------------------")
            for index in 0...numberToDelete-1 {
                println("Deleting entry with index \(entries[0].id)")
                realm.write { realm.delete(entries[0]) }
            }
            println("---------------------------------------")
            println("Deleted \(numberToDelete) entries from database.")
        }
        println("Currently \(entries.count)/\(entryCapacity) entries in memory.")
    }

}

