//
//  LogViewController.swift
//  Body
//
//  Created by Daniel Li on 7/21/15.
//  Copyright (c) 2015 Daniel. All rights reserved.
//

import UIKit
import RealmSwift

class TodayLogViewController: UITableViewController {

    var entries: Results<Entry> {
        get {
            return Realm().objects(Entry).sorted("timeStampNSDate", ascending: false)
        }
    }
    
    var todayEntries = [Entry]()
    
    var selectedEntry = Entry()
    
    @IBOutlet weak var closeButton: UIBarButtonItem!
    @IBOutlet weak var viewAllButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 44.0;
        if let font = UIFont(name: "Avenir", size: 16) {
            viewAllButton.setTitleTextAttributes([NSFontAttributeName: font], forState: UIControlState.Normal)
            closeButton.setTitleTextAttributes([NSFontAttributeName: font], forState: UIControlState.Normal)
        }
        
        // Put entry in todayEntries array if it's from today
        let calendar = NSCalendar.currentCalendar()
        for entry in entries {
            if calendar.isDateInToday(entry.timeStampNSDate) {
                todayEntries.append(entry)
            }
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return todayEntries.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let selectedEntryIndex = find(todayEntries, selectedEntry) {
            if indexPath.row == selectedEntryIndex + 1 {
                let cell = tableView.dequeueReusableCellWithIdentifier("commentsCell", forIndexPath: indexPath) as! CommentsCell
                cell.commentsLabel!.text = selectedEntry.comments
                cell.userInteractionEnabled = false
                return cell
            }
        }
        let entry = todayEntries[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("entryCell", forIndexPath: indexPath) as! EntryCell
        
        cell.nameLabel?.text = entry.name
        cell.timeLabel?.text = entry.timeStampTimeString
        cell.backgroundColor = UIColor.whiteColor()
        cell.accessoryType = .None
        cell.selectionStyle = .None
        cell.commentsImage.hidden = true
        if !entry.comments.isEmpty {
            cell.commentsImage.hidden = false
            cell.selectionStyle = .Default
        }
        if let selectedEntryIndex = find(todayEntries, selectedEntry) {
            if indexPath.row == selectedEntryIndex {
                cell.separatorInset = UIEdgeInsetsMake(0, 0, cell.bounds.size.height, cell.bounds.size.width)
            }
        }
        return cell
    }
    
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        // If there's row that's already selected, selectedIndexPath is its indexPath
        if let selectedIndexPath = tableView.indexPathForSelectedRow() {
            // Deselect the row that's already selected...
            tableView.deselectRowAtIndexPath(selectedIndexPath, animated: true)
            
            // Delete comments row below it from the table
            let deleteIndexPath = NSIndexPath(forRow: selectedIndexPath.row + 1, inSection: selectedIndexPath.section)
            todayEntries.removeAtIndex(selectedIndexPath.row + 1)
            tableView.deleteRowsAtIndexPaths([deleteIndexPath], withRowAnimation: .Top)
            
            // And set the selected entry to default
            selectedEntry = Entry()
            
            // Don't select it
            return nil
        }
        if todayEntries[indexPath.row].comments.isEmpty {
            return nil
        }
        // Otherwise, select the row.
        selectedEntry = todayEntries[indexPath.row]
        return indexPath
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if !selectedEntry.comments.isEmpty {
            let insertIndexPath = NSIndexPath(forRow: indexPath.row + 1, inSection: indexPath.section)
            todayEntries.insert(Entry(), atIndex: indexPath.row + 1)
            tableView.insertRowsAtIndexPaths([insertIndexPath], withRowAnimation: .Top)
        }
    }
    

    
    @IBAction func todayLogUnwind(segue:UIStoryboardSegue) {
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let selectedIndexPath = tableView.indexPathForSelectedRow() {
            // Deselect the row that's already selected...
            tableView.deselectRowAtIndexPath(selectedIndexPath, animated: true)
            
            // Delete comments row below it from the table
            let deleteIndexPath = NSIndexPath(forRow: selectedIndexPath.row + 1, inSection: selectedIndexPath.section)
            todayEntries.removeAtIndex(selectedIndexPath.row + 1)
            tableView.deleteRowsAtIndexPaths([deleteIndexPath], withRowAnimation: .Top)
            
            // And set the selected entry to default
            selectedEntry = Entry()
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
