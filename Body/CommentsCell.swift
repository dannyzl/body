//
//  CommentsCell.swift
//  Body
//
//  Created by Daniel Li on 7/22/15.
//  Copyright (c) 2015 Daniel. All rights reserved.
//

import UIKit

class CommentsCell: UITableViewCell {

    @IBOutlet weak var commentsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        commentsLabel.numberOfLines = 0
        commentsLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
