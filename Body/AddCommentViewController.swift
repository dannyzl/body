//
//  AddCommentViewController.swift
//  Body
//
//  Created by Daniel Li on 7/24/15.
//  Copyright (c) 2015 Daniel. All rights reserved.
//

import UIKit
import RealmSwift

class AddCommentViewController: UITableViewController {
    
    var entry = Entry()
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    @IBAction func cancelButton(sender: UIBarButtonItem) {
        entry.id = entry.newEntryID()
        let realm = Realm()
        realm.write {
            realm.add(self.entry)
        }
        println("\(entry.name) logged with id = \(entry.id) and time stamp: \(entry.timeStampStringShort)")
        performSegueWithIdentifier("unwindHome", sender: self)
    }
    
    @IBAction func addButton(sender: UIBarButtonItem) {
        entry.comments = commentField.text
        entry.id = entry.newEntryID()
        let realm = Realm()
        realm.write {
            realm.add(self.entry)
        }
        println("\(entry.name) logged with id = \(entry.id), time stamp: \(entry.timeStampStringShort), and with comments: \"\(entry.comments)\"")
        performSegueWithIdentifier("unwindHome", sender: self)

    }
    @IBOutlet weak var commentField: UITextField!
    
    @IBAction func commentChanged(sender: UITextField) {
        if sender.text.isEmpty {
            addButton.enabled = false
        } else {
            addButton.enabled = true
        }
        commentField.text = removePrefixSpaces(commentField.text)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addButton.enabled = false
        commentField.becomeFirstResponder()
        // Appearance
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
        if let font = UIFont(name: "Avenir", size: 16) {
            cancelButton.setTitleTextAttributes([NSFontAttributeName: font], forState: UIControlState.Normal)
        }
        if let font = UIFont(name: "Avenir-Medium", size: 16) {
            addButton.setTitleTextAttributes([NSFontAttributeName: font], forState: UIControlState.Normal)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 1
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        commentField.becomeFirstResponder()
    }
    
    func removePrefixSpaces(var input: String) -> String {
        if input.hasPrefix(" ") {
            input.removeAtIndex(input.startIndex)
            removePrefixSpaces(input)
        }
        return input
    }
    
}
